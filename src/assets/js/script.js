//Silbentrennung konfiguration
//- This repository replaces https://code.google.com/p/hyphenator/
//- Demo: http://mnater.github.io/Hyphenator/
//
//Note: Hyphenator.js has somewhat grown old. Have a look at its successor: https://github.com/mnater/Hyphenopoly
//
 
Hyphenator.config({
  hyphenchar: '-',
  minwordlength : 2
});

//bei jedem Tastenanschalg wird das Script ausgeführt
$('#text').keyup(function(){
  var text = $('#text').val();//Text als Variable
  text = text.replace(/(?:\r\n|\r|\n)/g, ' ');//Umbrüche durch Leerzeichen ersetzen
  text = Hyphenator.hyphenate(text, 'de');//Silbentrennung wird ausgeführt
console.log(text)
  var words = text.split(' ');//Text wird anhand der Leerzeichen in Wörter aufgesplittet

	console.log(words);
  var silben = [];// neues Array
  for (var i = 0; i < words.length; i++){
    var wort = words[i];
    var wortssilben = wort.split('-');
    silben = silben.concat(wortssilben);//fügt zwei Arrays mit Silben zusammen
  }
	console.log(silben)
	//Ausgabe gedrehte Buchstaben
  $('#output').html('');
  for (var i = 0; i < silben.length; i++){
    var html = '';
    var buchstaben = silben[i].split('');//gibt die einzelnen Buchstaben in einem Array zurück
	  
	//+= fügt etwas hinzu
	//number= Stelle des Buchstabens in der Silbe Buchstabe 1=0, Buchstabe 2=1
    for(var j = 0; j< buchstaben.length;j++){
        html += '<div class="buchstabe buchstabe-'+j+' buchstabe-'+buchstaben[j].toLowerCase()+'" data-number="'+j+'">'+buchstaben[j]+'</div>';
    }

	//append (jquery) = hinzufügen
    $('#output').append('<div class="icon">'+html+'</div>');
  }
  $('.buchstabe').each(function(){
    $(this).rotate($(this).data('number')*45,1000);
  });
});

 
$('#text').keyup()
